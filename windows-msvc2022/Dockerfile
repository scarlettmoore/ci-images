#escape=`
FROM mcr.microsoft.com/windows/server:ltsc2022

LABEL Description="Windows Base for use with Craft"
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

ENV chocolateyUseWindowsCompression=false

# Visual Studio is a bit delicate, ensure it is running under cmd.exe for maximum compatibility
SHELL ["cmd.exe", "/S", "/C"]

# Based on https://docs.microsoft.com/en-us/visualstudio/install/build-tools-container
# Install Build Tools with the vc++ workload, excluding workloads and components with known issues.
RUN `
    # Download the latest Build Tools bootstrapper
    curl -SL --output vs_buildtools.exe https://aka.ms/vs/17/release/vs_buildtools.exe  `
    `
    # Install Visual Studio with the components we need, excluding workloads and components with known issues.
    # We instruct it to not update the installer as that just causes too many issues
    && (start /w vs_buildtools.exe --quiet --wait --norestart --nocache --noUpdateInstaller `
        --channeluri https://aka.ms/vs/17/release/channel `
        --installchanneluri https://aka.ms/vs/17/release/channel `
        --add Microsoft.VisualStudio.Component.VC.ATL `
        --add Microsoft.VisualStudio.Component.VC.Tools.x86.x64 `
        --add Microsoft.VisualStudio.Component.VC.CoreBuildTools `
        --add Microsoft.VisualStudio.Component.VC.CLI.Support `
        --add Microsoft.VisualStudio.Component.Windows10SDK `
        --add Microsoft.VisualStudio.Component.Windows10SDK.20348 `
        --add Microsoft.VisualStudio.Component.Windows11SDK `
        --add Microsoft.VisualStudio.Component.Windows11SDK.22621 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.10240 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.10586 `
        --remove Microsoft.VisualStudio.Component.Windows10SDK.14393 `
        --remove Microsoft.VisualStudio.Component.Windows81SDK `
        || IF "%ERRORLEVEL%"=="3010" EXIT 0) `
    `
    # Cleanup
    && del /q vs_buildtools.exe `
    && powershell -Command "Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose -ErrorAction SilentlyContinue"

# Switch to Powershell from here on now that Visual Studio is all happy
SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# Install chocolatey, then use it to install Git, Python 2 and 3, 7zip and Powershell Core
RUN iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1')); `
    choco feature disable --name=showDownloadProgress; `
    # Pin Python3 to 3.11 because QtWebEngine is not compatible with 3.12 yet
    choco install -y python3 --version=3.11.6; `
    choco install -y git 7zip powershell-core; `
    choco install -y msys2 --params '/NoUpdate /InstallDir:C:\MSys2'; `
    Remove-Item @( 'C:\*Recycle.Bin\S-*' ) -Force -Recurse -Verbose; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose -ErrorAction SilentlyContinue;

# Switch to Powershell Core from here on now that its installed
SHELL ["pwsh", "-Command", "Set-StrictMode -Version Latest; $ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue'; $PSNativeCommandUseErrorActionPreference = $true;"]
CMD ["pwsh", "-NoExit", "-Command", "Set-StrictMode -Version Latest; $ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue'; $PSNativeCommandUseErrorActionPreference = $true;"]

# Install a couple of Python bindings needed by the CI Tooling
# * ci-notary-service client scripts need paramiko, pyyaml, requests
RUN pip install lxml pyyaml python-gitlab packaging paramiko requests; `
    Remove-Item @( 'C:\Windows\Temp\*', 'C:\Windows\Prefetch\*', 'C:\Documents and Settings\*\Local Settings\temp\*', 'C:\Users\*\Appdata\Local\Temp\*' ) -Force -Recurse -Verbose -ErrorAction SilentlyContinue;

# Make sure Git does not attempt to use symlinks as they don't work well with CMake and co
RUN git config --system core.symlinks false
